﻿namespace TC5
{
    class Nodo
    {
        public int id;
        public string nombre;
        public string apellido;
        public Nodo izquierdo;
        public Nodo derecho;
    }
}
