﻿using System;

namespace TC5
{
    class Program
    {
        static void Main(string[] args)
        {
            ArbolB fichas = new ArbolB();
            fichas.InsertarNodo(5, "Davy", "Hipólito");
            fichas.InsertarNodo(2, "Hugo", "Huatuco");
            fichas.InsertarNodo(10, "Ana", "Saavedra");
            fichas.InsertarNodo(100, "Miguel", "Garibay");
            fichas.InsertarNodo(1, "Agustin", "Llacsahuache");
            fichas.InsertarNodo(3, "Peter", "Montalvo");

            Console.WriteLine("\nLista de pacientes INORDER:\n--------------------------");
            fichas.Inorden(fichas.GetRaiz());

            Console.WriteLine("\nLista de pacientes PREORDER:\n--------------------------");
            fichas.Preorden(fichas.GetRaiz());

            Console.WriteLine("\nLista de pacientes POSTORDER:\n--------------------------");
            fichas.Postorden(fichas.GetRaiz());

            Console.WriteLine("\nEliminando pacientes con ID:1 y ID:100...");
            fichas.Eliminar(fichas.GetRaiz(), 1);
            fichas.Eliminar(fichas.GetRaiz(), 100);

            Console.WriteLine("\nLista de pacientes INORDER:\n--------------------------");
            fichas.Inorden(fichas.GetRaiz());

            Console.WriteLine("\nLista de pacientes PREORDER:\n--------------------------");
            fichas.Preorden(fichas.GetRaiz());

            Console.WriteLine("\nLista de pacientes POSTORDER:\n--------------------------");
            fichas.Postorden(fichas.GetRaiz());

            Console.Read();
        }
    }
}
