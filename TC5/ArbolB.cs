﻿using System;

namespace TC5
{
    class ArbolB
    {
        public Nodo raiz;

        public Nodo GetRaiz()
        {
            return raiz;
        }
        public void InsertarNodo(int id, string nombre , string apellido)
        {
            Nodo puntero;
            Nodo padre;
            Nodo nodo = new Nodo {id = id, nombre = nombre, apellido = apellido};
            if (raiz != null)
            {
                puntero = raiz;
                Console.WriteLine("Agregando nuevo paciente con  ID: {0} ...",id);
                while (true)
                {
                    padre = puntero;
                    if (id < puntero.id)
                    {
                        puntero = puntero.izquierdo;
                        if (puntero == null)
                        {
                            padre.izquierdo = nodo;
                            break;
                        }
                    }
                    else
                    {
                        puntero = puntero.derecho;
                        if (puntero == null)
                        {
                            padre.derecho = nodo;
                            break;
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("Agregando primer paciente con ID: {0} ...",nodo.id);
                raiz = nodo;
            }
        }
        public void Inorden(Nodo raiz)
        {
            if (raiz != null)
            {
                Inorden(raiz.izquierdo);
                Console.WriteLine("ID: {0}, {1} {2}", raiz.id, raiz.nombre,raiz.apellido);
                Inorden(raiz.derecho);
            }
        }
        public void Preorden(Nodo raiz)
        {
            if (raiz != null)
            {
                Console.WriteLine("ID: {0}, {1} {2}", raiz.id, raiz.nombre, raiz.apellido);
                Preorden(raiz.izquierdo);
                Preorden(raiz.derecho);
            }
        }
        public void Postorden(Nodo raiz)
        {
            if (raiz != null)
            {
                Postorden(raiz.izquierdo);
                Postorden(raiz.derecho);
                Console.WriteLine("ID: {0}, {1} {2}", raiz.id, raiz.nombre, raiz.apellido);
            }
        }
        public Nodo Eliminar(Nodo puntero, int llave)
        {
            if (puntero == null)
            {
                return puntero;
            }
            if (llave < puntero.id)
            {
                puntero.izquierdo = Eliminar(puntero.izquierdo, llave);
            }
            if (llave > puntero.id)
            {
                puntero.derecho = Eliminar(puntero.derecho, llave);
            }
            if (llave == puntero.id)
            {
                if (puntero.izquierdo == null && puntero.derecho == null)
                {
                    puntero = null;
                    return puntero;
                }
                else if (puntero.izquierdo == null)
                {
                    Nodo temp = puntero;
                    puntero = puntero.derecho;
                    temp = null;
                }
                else if (puntero.derecho == null)
                {
                    Nodo temp = puntero;
                    puntero = puntero.izquierdo;
                    temp = null;
                }
            }
            return puntero;
        }


    }
}
