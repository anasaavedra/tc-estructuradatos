﻿using System;

namespace TC7
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] graph = {
               {0,6,5,0,0,0,0 },
               { 6,0,8,16,0,0,0},
               { 5,8,0,0,0,0,6},
               { 0,16,0,0,1,8,9},
               { 0,0,0,1,0,0,0},
               { 0,0,0,8,0,0,5},
               { 0,0,6,9,0,5,0}
            };

            Prim.PrimAlgo(graph, 7);

            Console.Read();
        }
    }
}
