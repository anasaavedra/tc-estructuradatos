﻿using System;

namespace TC3
{
    class Program
    {
        static void Main(string[] args)
        {
            Nodo final = null;
            ListaCircular jugadores = new ListaCircular();
            final = jugadores.AgregarNodoAlFinal(final, "1", "Juan", 150);
            final = jugadores.AgregarNodoAlFinal(final, "2", "Miguel", 250);
            final = jugadores.AgregarNodoAlFinal(final, "3", "Maria", 350);
            final = jugadores.AgregarNodoAlFinal(final, "4", "Julio", 100);
            final = jugadores.AgregarNodoAlFinal(final, "5", "Teresa ", 50);

            Console.WriteLine();

            jugadores.RecorrerLista(final);
            final = jugadores.Eliminar(final, "3");
            final = jugadores.Eliminar(final, "4");
            jugadores.RecorrerLista(final);

            final = jugadores.AgregarNodoAlFinal(final, "6", "Katy ", 150);
            final = jugadores.AgregarNodoAlFinal(final, "7", "Frank ", 220);
            jugadores.RecorrerLista(final);
            Console.ReadLine();
        }
    }
}
