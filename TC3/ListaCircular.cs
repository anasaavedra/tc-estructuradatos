﻿using System;

namespace TC3
{
    class ListaCircular
    {
        public void RecorrerLista(Nodo final)
        {
            Console.WriteLine("\n------     Lista de Jugadores     -------");
            Nodo actual;
            if (final == null)
            {
                Console.WriteLine("La lista está vacía");
                return;
            }
            actual = final.siguiente;
            do
            {
                Console.WriteLine("ID: " + actual.id + "\tNombre: " + actual.nombre + "\tPuntaje: " + actual.puntos);
                actual = actual.siguiente;
            } while (actual != final.siguiente);
            Console.WriteLine("-----------------------------------------\n");
        }
        public Nodo AgregarAListaVacia(Nodo final, string id,string nombres,int puntos)
        {
            if (final != null)
            {
                return final;
            }
            Console.WriteLine("Agregando a la lista el valor inicial: " + nombres);
            Nodo temporal = new Nodo();
            temporal.id = id;
            temporal.nombre=nombres;
            temporal.puntos = puntos;
            final = temporal;
            final.siguiente = final;
            return final;
        }
        
        public Nodo AgregarNodoAlFinal(Nodo final, string id, string nombres, int puntos)
        {
            if (final == null)
            {
                return AgregarAListaVacia(final, id,nombres,puntos);
            }
            Console.WriteLine("Agregando al final de la lista: " + nombres);
            Nodo temporal = new Nodo();
            temporal.id = id;
            temporal.nombre = nombres;
            temporal.puntos = puntos;
            temporal.siguiente = final.siguiente;
            final.siguiente = temporal;
            final = temporal;
            return final;
        }
            

        public Nodo Eliminar(Nodo fin, string id)
        {
            if (fin == null)
            {
                Console.WriteLine("\nLista vacía - Nada que eliminar");
                return null;
            }
            else
            {
                Nodo temp = fin;
                while (temp.siguiente.id != id)
                {
                    temp = temp.siguiente;
                    if (temp == fin)
                    {
                        Console.WriteLine("\nNo se pudo eliminar - ID no encontrado");
                        temp = null;
                        return fin;
                    }
                }
                Console.WriteLine("Eliminando a " + temp.siguiente.nombre + " de la lista de jugadores...");
                if (temp.siguiente == fin)
                {
                    fin = temp;
                }
                temp.siguiente = temp.siguiente.siguiente;
                return fin;
            }
        }
    }
}
