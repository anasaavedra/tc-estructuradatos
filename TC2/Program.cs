﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TC2
{
    class Program
    {
        static void Main(string[] args)
        {
            ListaDoble Rappid = new ListaDoble();
            Rappid.AddFinal(1250, "Pachamanca", "Ca. Los Laureles s/n - San Isidro"); //Si la lista esta vacía, AddFinal, AddInicio crearan la lista
            
            Rappid.AddInicio(1420, "Patitas de conejo", "Av. Los Alisos 179 - Comas");
            Rappid.AddInicio(2563, "Picante de Cuy", "Av. Tingo María 1122- Cercado de Lima");
            Rappid.AddInicio(1010, "Ceviche de Calamar", "Av. Universitaria Km 25 - Carabayllo");
            Rappid.AddInicio(1522, "Parrilla de Cerdo", "Jr. Los Alcanfores 174 - Chorrillos");
            Rappid.Mostrar();
           
            Rappid.AddFinal(3069, "Ajiaco de carne", "Miraflores 1525 s/5");
            Rappid.AddFinal(4010, "Arroz Chaufa", "Av. México 454 - La Victoria");
            Rappid.AddFinal(35, "Cau Cau", "Los naranjo 175, Pueblo Libre");
            Rappid.Mostrar();
            
            Rappid.ALuegod(1012, "Arroz con pato", "Av. Argentina 1120 - Callao", 1010);
            Rappid.Mostrar();
            Rappid.ALuegod(1251, "Papa a la huancaína", "Av. Brasil 477 - Jesús María",1250); //ID,Pedido,Direccion,Luedo de ID
            Rappid.Mostrar();
            Rappid.ALuegod(270, "Salchihueso", "Av. Venezuela 666",15);
            Rappid.Mostrar();
           
            Rappid.Eliminar(270);
            Rappid.Mostrar();
            Rappid.Eliminar(30);
           
            Rappid.EliPrim();
            Rappid.Mostrar();
            Rappid.EliFin();
            Rappid.Mostrar();
            Console.ReadKey();
        }
    }
}
