﻿using System;

namespace TC2
{
    class ListaDoble
    {
        public Nodo cabeza;
        public void AddInicio(int id, string pedido, string adress)
        {
            Nodo npedido = new Nodo(){ anterior = null, siguiente = null, Index = id, pedido = pedido, direccion = adress };
            if (cabeza==null) //si la lista vacia, igual lo agrega
            {
                cabeza = npedido;
                Console.WriteLine("Agregando pedido ID: "+id+" "+pedido+" a lista vacía...\n");
            }
            else
            {
                cabeza.anterior = npedido; //agrega pedido antes del primero
                npedido.siguiente = cabeza;
                cabeza = npedido;
                npedido = null;
                Console.WriteLine("Agregando nuevo pedido ID: "+id+" "+pedido+" al inicio...\n");
            }
        }

        public void AddFinal(int id, string pedido, string adress)
        {
            Nodo npedido = new Nodo() { anterior = null, siguiente = null, Index = id, pedido = pedido, direccion = adress };
            if (cabeza==null) //si la lista esta vacía, igual lo agrega
            {
                cabeza = npedido;
                Console.WriteLine("Agregando nuevo pedido ID: "+id+" "+pedido+" a lista vacía...\n");
            }
            else
            {
                Nodo aux = cabeza;
                while (aux.siguiente!= null)
                {
                    aux = aux.siguiente;
                }
                aux.siguiente = npedido;  //pedido agregado al final
                npedido.anterior = aux;
                Console.WriteLine("Agregando nuevo pedido ID: "+id+" "+pedido+" al final de la lista....\n");
            }

        }

        public void ALuegod(int id, string pedido, string adress, int referencia=0) //si no ingresa referencia, no importa, lo llevara al final
        {
            Nodo npedido = new Nodo() { anterior = null, siguiente = null, Index = id, pedido = pedido, direccion = adress };

            Console.WriteLine("Agregando nuevo pedido ID: "+id+" "+pedido+" despues del ID: "+referencia+" ...");
            if (cabeza == null) //si la lista esta vacia, agregara el pedido como unico de la lista
            {
                cabeza = npedido;
                Console.WriteLine("La lista esta vacía. Se agregó ID: "+id+" "+pedido+"como nuevo pedido\n");
            }
            else
            {
                Nodo aux = cabeza;
                 while (aux.Index != referencia)
                 {
                        aux = aux.siguiente;
                        if (aux == null)
                        {
                            break;
                        }
                 }
                if (aux == null)
                {
                   Console.WriteLine("Referencia no encontrada"); //si no ingresa referencia o no la encuentra, agregara el pedido al final de la lista
                   AddFinal(id, pedido, adress);
                }
                else
                {
                    Nodo temp = aux.siguiente; //agrega el pedido luego de la referencia
                    aux.siguiente = npedido;
                    npedido.anterior = aux;
                    npedido.siguiente = temp;
                    Console.WriteLine(pedido +" agregado a lista\n");
                }
            }
        }

        public void Mostrar()
        {
            Nodo aux = cabeza;
            if (aux==null)
            {
                Console.WriteLine("No hay pedidos que mostrar\n"); //si la lista esta vacía
            }
            else
            {
                Console.WriteLine("Mostrando pedidos...\n_________  LISTA DE PEDIDOS  _________\n"); //muestra pedidos uno debajo de otro
                while (aux != null)
                {
                    Console.WriteLine("ID\t : "+aux.Index + "\nDETALLE\t : " + aux.pedido + "\nDIRECCION: " + aux.direccion+"\n");
                    aux = aux.siguiente;
                }
            }
            
            Console.WriteLine("--------------------------------------\n");
        }
        public void EliPrim()
        {
            if (cabeza==null)
            {
                Console.WriteLine("Lista vacía. Nada que eliminar\n"); //si la lista esta vacia no elimina nada
            }
            else
            {
                Console.WriteLine("Eliminado primer pedido...\n" +cabeza.pedido+" eliminado.\n");
                Nodo aux = cabeza.siguiente;
                aux.anterior = null;
                cabeza = aux;
                aux = null;
            }
        }
        public void EliFin()
        {
            if (cabeza==null)
            {
                Console.WriteLine("Lista vacía. Nada que eliminar\n"); //si lista vacia, nada q hacer
            }
            else
            {
                Nodo aux = cabeza;
                while (aux.siguiente!=null)
                {
                    aux = aux.siguiente;
                }
                Console.WriteLine("Eliminando ultimo pedido...\n" +aux.pedido+" eliminado.\n"); //muestra mensje antes de limpiar el aux
                Nodo temp = aux.anterior;
                temp.siguiente = null;
                aux = null;

            }
        }

        public void Eliminar(int nume)
        {
            Console.WriteLine("Buscando pedido ID: " + nume);
            Nodo aux = cabeza;
            if (aux == null)
            {
                Console.WriteLine("Lista vacia: Nada que eliminar"); //comprobando que la lista no este vacía
                aux = null;
            }
            else
            {
                while (aux.Index != nume)
                {
                    aux = aux.siguiente; //aux busca la referencia, 
                    if (aux == null)
                    {
                        break;
                    }
                }
                if (aux == null)
                {
                    Console.WriteLine("No se pudo eliminar este pedido pues no encuentra en la lista.\n"); //si no la encuentra no hace nada
                }
                else
                {
                    Console.Write("Eliminando... " + aux.pedido + "\n");
                    Nodo anteraux = aux.anterior;
                    Nodo siguiaux = aux.siguiente;
                    if (anteraux == null && siguiaux != null) //si el pedido esta en primer lugar
                    {
                        cabeza = cabeza.siguiente;
                        cabeza.anterior = null;
                        aux = null;
                        siguiaux = null;
                    }
                    else if (anteraux == null && siguiaux == null) //si la lista solo tiene un elemento, y es que se va a borrar
                    {
                        cabeza = null;
                        aux = null;
                    }
                    else if (anteraux != null && siguiaux == null) //si el elemento a borrar es el ultimo de la lista
                    {
                        anteraux.siguiente = siguiaux;
                        aux = null;
                    }
                    else //lo demas
                    {
                        anteraux.siguiente = siguiaux;
                        siguiaux.anterior = anteraux;
                        aux = null;
                    }
                    Console.WriteLine("Pedido eliminado\n");
                }

            }
        }
    }
}
