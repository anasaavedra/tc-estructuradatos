﻿using System;

namespace TC6_v2
{
    class FloydWarshall
    {
        public const int INF = 99999;
        static string[] lugares = { "","San Miguel", "La Molina", "Callao", "San Borja", "Magdalena" };
        private static void Imprimir(int[,] distancia, int TotalVertices)
        {
            Console.WriteLine("Distancia mínima entre cada par de vértices:\n--------------------------------------------");
            for (int k = 0; k < lugares.Length; k++)
            {
                Console.Write(lugares[k].PadLeft(12));
            }
            Console.WriteLine("");
            for (int i = 0; i < TotalVertices; ++i)
            {
                Console.Write(lugares[i+1].PadLeft(12));
                for (int j = 0; j < TotalVertices; ++j)
                {
                    if (distancia[i, j] == INF)
                        Console.Write("INF".PadLeft(12));
                    else
                        Console.Write(distancia[i, j].ToString().PadLeft(12));
                }
                Console.WriteLine();
            }
        }

        public static void FloydWarshallAlgo(int[,] grafo, int TotalVertices)
        {
            int[,] distancia = new int[TotalVertices, TotalVertices];
            for (int i = 0; i < TotalVertices; ++i)
                for (int j = 0; j < TotalVertices; ++j)
                    distancia[i, j] = grafo[i, j];

            for (int k = 0; k < TotalVertices; ++k)
            {
                for (int i = 0; i < TotalVertices; ++i)
                {
                    for (int j = 0; j < TotalVertices; ++j)
                    {
                        if (distancia[i, k] + distancia[k, j] < distancia[i, j])
                            distancia[i, j] = distancia[i, k] + distancia[k, j];
                    }
                }
            }

            Imprimir(distancia, TotalVertices);
        }
    }
}
