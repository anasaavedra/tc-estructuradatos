﻿using System;

namespace TC6_v2
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] caso_01 =
            {
                { 0, 8 , 6, 0 , 0 },
                { 0, 0 , 0, 0 , 9 },
                { 0, 0 , 0, 16, 0 },
                { 8, 10, 0, 0 , 0 },
                { 0, 0 , 1, 0 , 0 },      
            };
            Dijkstra.DijkstraAlgo(caso_01, 0, 5);

            const int INF = 99999;
            Console.WriteLine("");

            int[,] caso_02 =
            {
                { 0  , 8  , 6  , INF, INF},
                { INF, 0  , INF, INF, 9  },
                { INF, INF, 0  , 16 , INF},
                { 8  , 10 , INF, 0  , INF},
                { INF, INF, 1  , INF, 0  },
            };
            FloydWarshall.FloydWarshallAlgo(caso_02, 5);

            Console.Read();
        }
    }
}
