﻿using System;

namespace TC6_v2
{
    class Dijkstra
    {
        static string[] lugares = { "San Miguel", "La Molina", "Callao", "San Borja", "Magdalena" };
        private static int DistanciaMinima(int[] distancia, bool[] ConjuntoCaminoMasCorto, int TotalVertices)
        {
            int min = int.MaxValue;
            int minIndex = 0;

            for (int v = 0; v < TotalVertices; ++v)
            {
                if (ConjuntoCaminoMasCorto[v] == false && distancia[v] <= min)
                {
                    min = distancia[v];
                    minIndex = v;
                }
            }

            return minIndex;
        }

        public static void Imprimir(int[] distancia, int TotalVertices)
        {
            
            Console.WriteLine("Lugar de llegada".PadRight(20)+"Distancia\n----------------    ---------");

            for (int i = 0; i < TotalVertices; ++i)
                Console.WriteLine(lugares[i].PadRight(20)+distancia[i]);
        }

        public static void DijkstraAlgo(int[,] grafo, int Inicio, int TotalVertices)
        {
            int[] distancia = new int[TotalVertices];
            bool[] ConjuntoCaminoMasCorto = new bool[TotalVertices];

            for (int i = 0; i < TotalVertices; ++i)
            {
                distancia[i] = int.MaxValue;
                ConjuntoCaminoMasCorto[i] = false;
            }

            distancia[Inicio] = 0;

            for (int count = 0; count < TotalVertices - 1; ++count)
            {
                int u = DistanciaMinima(distancia, ConjuntoCaminoMasCorto, TotalVertices);
                ConjuntoCaminoMasCorto[u] = true;

                for (int v = 0; v < TotalVertices; ++v)
                    if (!ConjuntoCaminoMasCorto[v] && Convert.ToBoolean(grafo[u, v]) &&
                        distancia[u] != int.MaxValue && distancia[u] + grafo[u, v] < distancia[v])
                        distancia[v] = distancia[u] + grafo[u, v];
            }
            Console.WriteLine("Distancias desde "+lugares[Inicio]);
            Imprimir(distancia, TotalVertices);
        }
    }
}
