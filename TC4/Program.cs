﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TC4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Consultora: Yuki Consulting\n");
            ColaPrioridad.Nodo ColaP;
            ColaP = ColaPrioridad.Nuevo("Juan", "Ramirez", "N0010P", 1);
            Console.WriteLine("Ingresando primer paciente "+ColaP.nombre+" "+ColaP.apellido+" ...");
            ColaP = ColaPrioridad.push(ColaP, "Marco", "Polo", "N0025A", 2);
            ColaP = ColaPrioridad.push(ColaP, "Hugo", "Cuba", "N0075E", 3);
            ColaP = ColaPrioridad.push(ColaP, "Ana", "Saenz", "N00251B", 4);
            ColaP = ColaPrioridad.push(ColaP, "Carlos", "Tupac", "N0018C", 5);
            ColaPrioridad.mostrar(ColaP);
            ColaP = ColaPrioridad.pop(ColaP);
            ColaP = ColaPrioridad.pop(ColaP);
            ColaPrioridad.mostrar(ColaP);
            ColaP = ColaPrioridad.push(ColaP, "Alberto", "Rodriguez", "N0011A", 0);
            ColaPrioridad.mostrar(ColaP);
            ColaP = ColaPrioridad.pop(ColaP);
            ColaP = ColaPrioridad.pop(ColaP);
            ColaPrioridad.mostrar(ColaP);
            Console.ReadKey();
        }
    }
    class ColaPrioridad
    {
        public class Nodo
        {
            public string nombre, apellido, id;
            public int prioridad;
            public Nodo siguiente;
        }
        public static Nodo Nuevo(string nombre, string apellido, string id, int prioridad)
        {
            Nodo temp = new Nodo() { nombre = nombre, apellido = apellido, id = id, prioridad = prioridad, siguiente = null };
            return temp;
        }
        public static Nodo pop(Nodo cabeza)
        {
            if (cabeza==null)
            {
                Console.WriteLine("No hay mas pacientes en cola");
                return null;
            }
            Console.WriteLine("\nEl paciente "+cabeza.nombre+" "+cabeza.apellido+" fue atendido...");
            cabeza = cabeza.siguiente;
            return cabeza;
        }
        public static Nodo push(Nodo cabeza, string nombre, string apellido, string id, int prioridad)
        {
            Nodo puntero = cabeza;
            Nodo temp = Nuevo(nombre, apellido, id, prioridad);
            if (cabeza.prioridad > prioridad)
            {
                Console.WriteLine("\nIngresando "+temp.nombre+" "+temp.apellido+", paciente con alta prioridad");
                temp.siguiente = cabeza;
                cabeza = temp;
            }
            else
            {
                while (puntero.siguiente != null && puntero.siguiente.prioridad < prioridad)
                {
                    puntero = puntero.siguiente;
                }
                temp.siguiente = puntero.siguiente;
                puntero.siguiente = temp;
                Console.WriteLine("Ingresando en cola al paciente "+temp.nombre+" "+temp.apellido+" ...");
            }
            return cabeza;
        }
        public static void mostrar(Nodo cabeza)
        {
            Nodo temp = cabeza;
            if (temp == null)
            {
                Console.WriteLine("Cola Vacía");
            }
            Console.WriteLine("\nLista de Pacientes en espera:\n-----------------------------");
            while (temp != null)
            {
                Console.WriteLine("ID: " + temp.id + "\tNombres: " + temp.nombre + " " + temp.apellido);
                temp = temp.siguiente;
            }
        }
    }
}
